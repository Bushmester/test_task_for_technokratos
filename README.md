# Тестовое Задание Технократия

Нужно написать Ansible плейбук, который установит на сервер пакеты `git`, `docker`, `htop` и `docker-compose`. Далее нужно поднять контейнер с `postgres` и создать 3 пользователей Linux с разными паролями.

## Установка
Установите Ansible. [Тык](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Конфигурация

**Настройка Ansible**: `cp ansible.cfg.example ansible.cfg` - скопировать файл с примером настроек Ansible. В скопированный файл нужно прописать *inventory* - путь до файла *hosts*

**Настройка hosts**: создайте файл *hosts*, где будет находится информация о ваших хостах (например, наименование и ip адреса)

**Настройка playbook.yaml**: в файле *db/defaults/main.yaml* укажите в переменной *project_src_path* путь до Вашего проекта на хосте 

## Запуск
`ansible-playbook -i hosts playbook.yaml`

### Различия между v1.0 и v2.0
В ветке **v1.0** все расположено в одном файле *playbook.yaml*

В ветке **v2.0** все разбито по ролям:
1. Роль **Install Packages** устанавливает на сервер пакеты `git`, `docker`, `htop` и `docker-compose`
2. Роль **DB** поднимает контейнер с `postgres`
3. Роль **Create Users** создает трех пользователей Linux с разными паролями
